
### Prepare environment 
Please run test under pipenv for reducing environmental dependencies error.

* Install pipenv
```
brew update && brew install pipenv
```
* Clone this project and go to the project folder
* Activate pipenv
```
pipenv shell
```
* Install required libs under pipenv
```
pipenv install
```
(You can check installed libs under current pipenv)
```
pipenv graph
```
(Exit current pipenv)
```
exit
```
(Delete pipenv)
```
pipenv --rm
```




### Execute test
* Execute all test cases
```
pytest open_web.py 
```


